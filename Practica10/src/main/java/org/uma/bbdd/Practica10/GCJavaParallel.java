package org.uma.bbdd.Practica10;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import scala.Tuple2;

public class GCJavaParallel {
    public static void main(String[] args) {
        String fileName = "serialized.txt";

        try(Stream<String> stream = Files.lines(Paths.get(fileName))) {
        	
            long initTime = System.currentTimeMillis();

            List<Tuple2<Double, String>> output = stream
            		.parallel()
                    .map(line -> {
                        String[] var = line.split(":");
                        return new Tuple2<>((((double) var[1].length() - (double) var[1].replaceAll("[CG]", "").length()) / (double) var[1].length()) * 100, var[0]);
                    }).sorted(Comparator.comparing(Tuple2::_1)).collect(Collectors.toList());


            for (Tuple2<?, ?> tuple : output.subList(output.size()-1, output.size())) {
                System.out.println((tuple._2() + ": " + tuple._1()));
            }
            long endTime = System.currentTimeMillis();

            System.out.println("Tiempo = " + (endTime-initTime) + "ms");
        }catch (IOException e){
            e.printStackTrace();
        }




}
}