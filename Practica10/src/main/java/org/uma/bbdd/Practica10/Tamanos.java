package org.uma.bbdd.Practica10;




import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;

import ch.epfl.lamp.fjbg.JConstantPool.Entry;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import scala.Tuple2;

/**
 * Example implementing the Spark version of the "Hello World" Big Data program: counting the
 * number of occurrences of words in a set of files.
 *
 * @author Antonio J. Nebro <antonio@lcc.uma.es>
 */

public class Tamanos{

	public static void main(String[] args) throws IOException {
    // Step 1: parameter checking and log configuration
		if (args.length < 1) {
      throw new RuntimeException("Syntax Error: there must be one argument (a file name or a directory)");
		}

		Logger.getLogger("org").setLevel(Level.OFF) ;

		// STEP 2: create a SparkConf object
		SparkConf sparkConf = new SparkConf().setAppName("Spark Word count") ;

		// STEP 3: create a Java Spark context
		JavaSparkContext sparkContext = new JavaSparkContext(sparkConf) ;

    
    
    
	JavaRDD<String> lines = sparkContext.textFile(args[0]);
	
	 
	 JavaPairRDD<Integer, String> tuples1 = lines.mapToPair(s -> {
	      String[] fields = s.split(":");
	      return new Tuple2<>(fields[1].length(),fields[0] );
	    }).cache();
	 
	 List<Tuple2<Integer, String>> first = tuples1.
	            sortByKey().take(1);
	 List<Tuple2<Integer, String>> last = tuples1.
	            sortByKey(false).take(1);
	 
for (Tuple2<?, ?> tuple : first.subList(0, 1)) {	  
		 
		 System.out.println(tuple._2() + ": " + tuple._1());
		 PrintWriter outputfile = new PrintWriter("no.fasta");
			
		    outputfile.print(tuple._2() + ": " + tuple._1());
		    outputfile.close(); 
	 
	 }
for (Tuple2<?, ?> tuple : last.subList(0, 1)) {	  
		 
		 System.out.println(tuple._2() + ": " + tuple._1());
		 PrintWriter outputfile = new PrintWriter("N.fasta");
		
		    outputfile.print(tuple._2() + ": " + tuple._1());
		    outputfile.close(); 
	 
	 }


}
}

