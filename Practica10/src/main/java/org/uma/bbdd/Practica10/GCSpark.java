package org.uma.bbdd.Practica10;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;

import scala.Tuple2;

public class GCSpark {

	public static void main(String[] args) {
    // Step 1: parameter checking and log configuration
		if (args.length < 1) {
      throw new RuntimeException("Syntax Error: there must be one argument (a file name or a directory)");
		}

		Logger.getLogger("org").setLevel(Level.OFF) ;

		// STEP 2: create a SparkConf object
		SparkConf sparkConf = new SparkConf().setAppName("Spark Word count") ;

		// STEP 3: create a Java Spark context
		JavaSparkContext sparkContext = new JavaSparkContext(sparkConf) ;
		long initTime = System.currentTimeMillis();
    
    
    
	JavaRDD<String> lines = sparkContext.textFile(args[0]);
	
	JavaPairRDD<Integer,String> tuples = lines.mapToPair(s -> {
	      String[] fields = s.split(":");
	      
	      return new Tuple2<Integer,String>((fields[1].length()-fields[1].replaceAll("GC","").length())/fields[1].length() ,fields[0]);
	    }).cache();
	 List<Tuple2<Integer,String>> result = tuples.
	            sortByKey().collect();
	
	 
	 
	
	
	 
	 
	 for (Tuple2<?, ?> tuple : result.subList(0, 1)) {	  
		 
		 System.out.println(tuple._1() + ": " + tuple._2().toString().length());
	 
	 }
	 long endTime = System.currentTimeMillis();

     System.out.println("Tiempo = " + (endTime-initTime) + "ms");
	
	 

	

		// STEP 5: stop the spark context
		sparkContext.stop();
	}
	

}
