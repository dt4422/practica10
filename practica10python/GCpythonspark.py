

import sys
from pyspark import SparkConf, SparkContext
import time

from fastaread import save_fasta_serialized
if __name__ == "__main__":

    if len(sys.argv) != 2:
        print("Usage: spark-submit parallelize <file>", file=sys.stderr)
        exit(-1)

    #save_fasta_serialized(sys.argv[1])
    sparkConf = SparkConf()
    sparkContext = SparkContext(conf=sparkConf)
    start_time = time.time()
    sparkContext.setLogLevel("OFF")
    output1=sparkContext \
        .textFile(sys.argv[1]) \
        .map(lambda line: line.split(':')) \
        .map(lambda line:( ((len(str(line[1]))-len(str(line[1]).replace('G','').replace('C','')))/len(str(line[1]))),line[0]))\
        .takeOrdered(1,key = lambda x: -x[0])






print(output1)
print("--- %s seconds ---" % (time.time() - start_time))

