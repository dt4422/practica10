

import sys
from pyspark import SparkConf, SparkContext
import re
import operator
from fastaread import save_fasta_serialized
if __name__ == "__main__":

    if len(sys.argv) != 2:
        print("Usage: spark-submit parallelize <file>", file=sys.stderr)
        exit(-1)

    #save_fasta_serialized(sys.argv[1])
    sparkConf = SparkConf()
    sparkContext = SparkContext(conf=sparkConf)

    sparkContext.setLogLevel("OFF")




    output2 = sparkContext \
        .textFile('serialized.txt') \
        .map(lambda line: line.split(':')) \
        .map(lambda line: (line[0], len(line[1])))\
        .collect()
    fa = open('no.fasta', 'w')
    f=open('N.fasta','w')

    fa.write(str(min(output2,key=lambda x:x[1])))
    f.write(str(max(output2, key=lambda x: x[1])))





