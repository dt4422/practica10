import time

def GCpython(file):
    start_time = time.time()
    lines=[]
    with open(file) as inputfile:
        for line in inputfile:
            stre=line.split(':')

            c=(len(stre[1])-len(stre[1].replace('G','').replace('C','')))/(len(stre[1]))

            lines.append((stre[0],c*100))

    print(max(lines,key=lambda item:item[1]))
    print("--- %s seconds ---" % (time.time() - start_time))