# README #

This README would normally document whatever steps are necessary to get your application up and running.

*Esta practica tiene 2 partes:

*Python:
*fastaread Serializa e introduce id y > a cada linea de fichero
*GCpython Devuelve la secuencia con mayor % de GC usando python
*GCpythonspark Devuelve la secuencia con mayor % de GC usando Spark
*Addid Crea dos fichero uno con la secuencia mas grande y otro con la más pequeña

*Java:
*GCJava8 Devuelve la secuencia con mayor % de GC usando Java8
*GCJavaspark Devuelve la secuencia con mayor % de GC usando Spark
*GCJavaparallel Devuelve la secuencia con mayor % de GC usando paralelismo
*Tamanos Crea dos fichero uno con la secuencia mas grande y otro con la más pequeña

*Tiempos:
*GCpythonspark = 4sec
*GCpython = 1.8 sec
*GCJava8= 2 sec
*GCJavaParallel=1.5sec
*GCSpark=3,3 sec